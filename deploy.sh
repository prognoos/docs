rm -f site/docs.zip
mkdocs build
cd site
zip -r docs.zip .
cd ..
scp -i $PROGNOOS_SSH site/docs.zip ubuntu@api.prognoos.com:/tmp
ssh -i $PROGNOOS_SSH ubuntu@api.prognoos.com 'cd /home/ubuntu/apps && rm -rf old_docs && mv docs old_docs && mkdir docs && cd docs && mv /tmp/docs.zip . && unzip docs.zip'

