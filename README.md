# Prognoos Docs

## Environment

Python 3.4+ (works with 2.7)

Install `requirements.txt` dependencies.

## Deploy

Just run:

```sh
$ sh deploy.sh
```
