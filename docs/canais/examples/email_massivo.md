# Exemplo de email massivo

```html
<html>
  <head>
    <title>{{ title }}</title>
  </head>
  <body>
  <img src="https://api.prognoos.com/pxl.png/{{ cid }}?{{ params }}"
       width="0" height="0" />
  </body>
</html>
```
