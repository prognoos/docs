# Email

Nossa plataforma oferece comunicação por email. Todos os envios tem taxa de abertura
e cliques registrados e os resultados podem ser acompanhados no dashboard.

Temos 3 tipos de envios disponíveis para nossos clientes: massivo, segmentado e personalizado.

* Envio *massivo* - todos clientes recebem o mesmo email
* Envio *segmentado* - os clientes são separados por algum critério e cada segmento recebe um email específico
* Envio *personalizado* - cada cliente recebe um email individual

## Template

Quem define o template do email é o próprio cliente, mas temos parceiros que podem criar caso seja desejado.

Para criar um **Envio**, você precisa definir, obrigatoriamente:

* Tipo
* Título
* Código html do template

### Email massivo

Se você quer fazer um envio massivo, apenas as 3 informações obrigatórias
são necessárias. Os resultados podem ser acompanhados em tempo real, tanto aberturas
quanto cliques.

Existem 3 tags especiais, disponíveis para serem usadas nos templates. São elas:

* *cid* - Identificador do cliente
* *title* - Título definido na campanha
* *params* - Parametros passados para o pixel na hora de registrar a visualização

Um exemplo trivial de template de email (vazio):

```html
<html>
  <head>
    <title>{{ title }}</title>
  </head>
  <body>
  <img src="https://api.prognoos.com/pxl.png/{{ cid }}?{{ params }}"
       width="0" height="0" />
  </body>
</html>
```

### Email segmentado & Email personalizado

Apesar de ainda não estarem disponíveis, resolvemos explicar um pouco como eles
funcionarão: O envio segmentado quase tão simples quanto o massivo. 
A segmentação depende de atributos definidos por você. Nós temos algumas segmentações
básicas mas nossos clientes podem alimentar nosso sistema pra conseguir outras 
segmentações.

Os envios personalizados dependem de alguma forma de se escolher produtos para
clientes. Nós temos um conjunto de algoritmos que faz esse trabalho de forma automatizada.
Também, queremos permitir que nossos clientes, caso queiram, implementem suas estratégias
e rodem na nossa plataforma.

Na busca do retorno ótimo, o ideal é se pensar numa estratégia de comunicação e
não apenas nos tipos de envio. Uma vez criado um histórico de resultados de envio,
nossa plataforma também irá ajudar a entender como os envios podem ser sequenciados
de forma a aumentar o engajamento e conversão.
