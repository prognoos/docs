# Pixel de tracking

Existem casos onde a execução de código Javascript não é possível, como no caso de emails. Para acompanhar abertura de emails, por exemplo, o "pixel" se torna uma
ferramenta indispensável. O "pixel" nada mais é que uma URL para uma imagem de 1x1 px. Quando o cliente de email, por exemplo, tenta exibir a imagem do pixel,
nós registramos a requisição para utilizar na nossa plataforma de processamento de dados.

## Como usar o pixel?

Nossa plataforma oferece uma URL de "pixel" que pode conter parâmetros arbitrários de seu interesse.

Veja um exemplo de como incluir o pixel no conteúdo do seu email:

```html
<img src="https://api.prognoos.com/pxl.png/client_id?params" width="0" height="0" />
```

## Dúvidas frequentes

* O pixel pode ser usado em páginas da web que não emails? Sim. Geralmente isso não é feito pois fora dos emails, geralmente, o nosso código Javascript
pode ser executado e ele tem mais funcionalidades que apenas registrar o acesso.
