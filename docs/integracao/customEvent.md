# Eventos customizados

Em muitos casos, nossos clientes querem acompanhar métricas específicas em tempo real.
Tais métricas podem ser total de vendas, valor de faturamento etc. 

Para tal acompanhamento, é necessário usar nossa API de *eventos customizados*. A forma
padrão de se fazer isso é chamando uma função no objeto *tracker* da página.

## Envio de eventos customizados

Veja um exemplo:

```js
tracker.sendCustom({'revenue': 100.00})
```

Essa chamada registrará um evento de *receita* com o valor especificado. Esses valores
poderão ser acompanhados em tempo real no dashboard. Se os seus eventos forem compostos
de diversos atributos, buscaremos criar uma exibição que faça sentido pro seu caso.

Em caso de dúvida, [temos um exemplo completo aqui](examples/customEvent.md).
