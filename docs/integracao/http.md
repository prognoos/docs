# Eventos via HTTP

Em alguns casos específicos, alguns eventos podem ser gerados no *server-side* de nossos
clientes. Pensando nisso, fornecemos um endpoint HTTP, para que sejam postados eventos
customizados diretamente no nosso sistema de coleta de dados.

Você vai precisar de um token para ter acesso a esse serviço. Entre em contato para
obter o seu.

## Postando um evento

O que você deve fazer é um **POST** cujo corpo da requisição seja um **JSON** válido.
O exemplo abaixo mostra como fazer isso usando o utilitário *cURL*, pela linha de
comando:

```
$ curl -d '{"key": "seu_api_key", "id": "10222015SE", "revenue": "10.0"}' \
       -H "Content-Type: application/json"  \
       https://api.prognoos.com/custom/loja.com.br 
```

Não esqueça de trocar **loja.com.br** pelo seu próprio identificador.
