# Tracking de navegação

O componente mais essencial do Prognoos é a captura de dados de interação. O objetivo desse componente é coletar
informações sobre produtos acessados e outros eventos, como por exemplo conversão. Com esses dados nossos algoritmos
podem calcular quais produtos tem maior chance de receber atenção dos usuários que visitam sua loja.

## Que tipo de dados?

* Quem (email ou cookie) visitou um certo produto
* Quem (email ou cookie) visitou uma certa página de categoria de produtos

## Inclusão de script e meta-tag

Para capturar as informações de usuário e produtos (ou categoria) são necessários duas modificações no site cliente:

* Criar meta-tags de usuário, produto ou categoria
* Incluir JS de tracking de dados.

Para usar nosso script basta incluir, no final do html, logo antes ou depois da tag `</body>`

**Não esqueça** de trocar `loja.com.br` para o seu próprio domínio:

```html
<html>
  <head>
    <meta type="page" content="product_view" />

    <!-- Para tracking de usuários logados, adicione as tags abaixo -->
    <meta name="product:username" content="username" />
    <meta name="product:email" content="user@loja.com.br" />

    <!-- Para tracking de atributos de produto - OBRIGATORIOS -->
    <meta name="product:product_id" content="123456" />
    <meta name="product:category" content="Categoria" />
    <meta name="product:product_name" content="Um Produto" />
    <meta name="product:product_url" content="http://loja.com.br/produto/123456" />
    <meta name="product:product_img_url" content="http://images.loja.com.br/123456.jpg" />
    <meta name="product:price" content="220.00" />
    <meta name="product:priceSell" content="199.00" />
  </head>
  <body>
     ....
  </body>
  <script src="https://api.prognoos.com/static/e.js/loja.com.br"></script>
</html>
```

## Exemplo completo

Aqui temos [um exemplo completo](examples/tracker.md)

## Já integrei com o serviço de tracking

Agora você pode utilizar o serviço de [Vitrines dinâmicas e personalizadas](vitrine.md).
