# Vitrine

A Vitrine é o ponto de consumo das informações geradas pelo Prognoos. Hoje, contamos com diversos tipos de vitrines dinâmicas que podem ser
consumidas fácilmente via uma API Http. Cada tipo de vitrine tem uma lógica única por trás que leva em consideração os dados coletados pelo
componente de tracking.

## Lista de vitrines

* Recomendação por comportamento de navegação
* Recomendação por comportamento de compra (não disponível)
* Recomendação por similaridade entre itens
* Produtos vistos recentemente
* Produtos mais comprados recentemente

## Integração

A integração com nossa vitrine é feita usando o modelo [JSONP](https://en.wikipedia.org/wiki/JSONP). O processo de integração
depende de 2 tarefas:

1. Definição do markup html de exibição dos produtos
2. Inclusão do javascript que aponta pro endpoint `JSONP`

Em alguns casos, nós mesmos podemos codificar toda integração de vitrines mas nessa documentação tem toda informação pra que seja possível
consumir os dados de vitrine sem nossa intervenção.

## Definição do markup

Nosso serviço retorna uma lista de produtos recomendados, em formato json, e portanto, a exibição deles fica a cargo da
loja/plataforma que deseja a exibição dos mesmos. De maneira geral, o mais comum é gerar uma marcação (markup) pra cada
produto e adicionar eles em algum outro componente, como uma `<div>`.

## API de vitrine

O retorno das vitrines é igual independente do algoritmo escolhido. Veja a seguir um exemplo do retorno:

```json
[
  {
    "product_id": "1642",
    "price": "397.35",
    "img_url": "http://minhaloja.com.br/resized/458x458/images/produtos/produto.jpg",
    "url": "http://minhaloja.com.br/produtos/67554950",
    "id": "1642",
    "name": "LIP BALM ATACADO - 30 LIP BALMS",
    "priceSell": "397.35"
  }
]
```

### Recomendação para usuário

Os parâmetros obrigatórios tem nomes `cid` e `jsonp`. Abaixo, um exemplo de inclusão da tag `<script>` que pega os produtos
da nossa API. A função `exibeRecs` deve estar acessível no escopo global do ambiente Javascript e será invocada com uma lista de objetos recomendados.

Exemplo:

```html
<script type="text/javascript"
        src="https://cariri.prognoos.com/rec?cid=minhaloja.com.br&jsonp=exibeRecs">
```

### Recomendação por item

Os parâmetros obrigatórios tem nomes `cid`, `iid` e `jsonp`. Abaixo, um exemplo de inclusão da tag `<script>` que pega os produtos
da nossa API:

```html
<script type="text/javascript"
        src="https://cariri.prognoos.com/rec?cid=minhaloja.com.br&iid=123456789&jsonp=exibeRecs">
```

### Mais comprados recentemente


```html
<script type="text/javascript"
        src="https://cariri.prognoos.com/heuristic?cid=essenzedipozzi.com.br&heuristic=top_sellers&jsonp=exibeRecs">
```

### Mais visitados recentemente


```html
<script type="text/javascript"
        src="https://cariri.prognoos.com/heuristic?cid=essenzedipozzi.com.br&heuristic=top_visited&jsonp=exibeRecs">
```

### Exemplo completo

Veja um [exemplo completo e funcional de referência aqui](examples/vitrine.md).
