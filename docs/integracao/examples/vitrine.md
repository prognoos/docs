# Vitrine - implementação de exemplo

O código abaixo é uma **referência** de como implementar a integração sem dependencias adicionais. No mundo real,
nós implementariamos a parte de template de produto com alguma ferramenta como [Handlebars](http://handlebarsjs.com/), 
[Mustache](https://mustache.github.io/) ou [doT](http://olado.github.io/doT/index.html). Também é possível integrar, sem
restrições, com frameworks como [Angular](https://angularjs.org/) ou [Reactjs](https://facebook.github.io/react/).

```html
<html>
<body>
<h1>Vitrine</h1>
<div id="vitrine"></div>
<script type="text/javascript">
var product_template = '<div><h2><a href="##URL#">##NAME##<\/a><\/h2><div><img src="##IMG##"><\/div><\/div>';

function exibeRecs(recommendations) {
  var di = document.getElementById('vitrine');
  var elements_html = "";

  for (var i = 0; i < 5; i++) {
    var new_html = product_template.replace('##NAME##', recommendations[i].data_product_name);
    new_html = new_html.replace('##URL##', recommendations[i].data_product_url);
    new_html = new_html.replace('##IMG##', location.protocol + recommendations[i].data_product_img_url);
    elements_html += new_html;
  }
    di.innerHTML = elements_html;
}
</script>
<script src="https://cariri.prognoos.com/rec?cid=loja.com.br&jsonp=exibeRecs"></script>
</body>
</html>

```
