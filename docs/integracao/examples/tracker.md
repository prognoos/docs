# Captura de visualização de página de produto

```html
<html>
  <head>
    <meta type="page" content="product_view" />
    <!-- Para tracking de usuários logados, adicione as tags abaixo -->
    <meta name="product:username" content="username" />
    <meta name="product:email" content="user@loja.com.br" />

    <!-- Para tracking de atributos de produto - OBRIGATORIOS -->
    <meta name="product:product_id" content="123456" />
    <meta name="product:category" content="Categoria" />
    <meta name="product:product_name" content="Um Produto" />
    <meta name="product:product_url" content="http://loja.com.br/produto/123456" />
    <meta name="product:product_img_url" content="http://images.loja.com.br/123456.jpg" />
    <meta name="product:price" content="220.00" />
    <meta name="product:priceSell" content="199.00" />
    ...
  </head>
  <body>
    ...
  </body>
  <script src="https://api.prognoos.comstatic/e.js/loja.com.br"></script>
</html>
```
