# Exemplo de página com integração em uma página de produto

```html
<html>
  <head>
    <!-- Capturar tipo de página -->
    <meta type="page" content="product_view" />

    <!-- Capturar nome do usuário logado -->
    <meta name="user:username" content="Nome do usuário" />
    <!-- Capturar email do usuário logado -->
    <meta name="user:email" content="user@loja.com.br" />

    <!-- Capturar informações do produto visualizado -->
    <meta name="product:product_id" content="864353434" />
    <meta name="product:product_name" content="Um Produto" />
    <meta name="product:product_url" content="http://loja.com.br/product/1" />
    <meta name="product:product_img_url" content="http://loja.com.br/product/1.jpg" />
    <meta name="product:product_category_id" content="20" />

    <!-- (Opcional) Capturar informações do produto visualizado -->
    <meta name="product:product_availablilty" content="100" />
    <meta name="product:product_sale_price" content="20,90" />
    <meta name="product:product_original_price" content="22,00" />
  </head>
  <body>
    <h1>Produto</h1>
    ...
  </body>
  <script src="https://api.prognoos.comstatic/e.js/loja.com.br"></script>
  <script>
    tracker.trackProduct(); // dispara captura de visualização de produto
  </script>
</html>
```
