# Integração

Nosso serviço é composto de 2 componentes: Tracking de navegação e vitrines dinâmicas.

## Tracking de navegação

O [Tracking de navegação](integracao/tracking.md) é o responsável por coletar que produtos (ou outras páginas) estão sendo acessados por quais clientes pra que nossos
algoritmos possam, mais tarde, criar diversas listas de produtos tanto para segmentos de clientes quando para indivíduos. Essas listas são baseadas em vários critérios
que consideram comportamentos individuais e de segmentos de clientes para inferir que produtos são mais relevantes para cada usuário.

Mais informações em: [Tracking de navegação](integracao/tracking.md)

## Vitrines dinâmicas

As [Vitrines dinâmicas](integracao/vitrine.md) são responsáveis por exibir produtos para os clientes consultando nossa API.

Mais informações: [Personalização](integracao/vitrine.md)
