# Sobre a Prognoos

A prognoos é uma empresa que preza principalmente por:

* Qualidade dos serviços prestados aos clientes
* Qualidade ténica das suas ferramentas internas

Buscamos trabalhar com empresas que procuram um **serviço eficiente** e com o **preço justo**. Queremos que a nossa
plataforma **gere mais vendas** e **aumente a conversão** e **fortaleça o engajamento** com o seu produto. Acreditamos tanto no nosso
potencial que **nossa receita** é baseada em **resultados gerados**.

Apesar de ser uma empresa muito nova, temos alguns anos de experiência na indústria e formação acadêmica sólida
que nos permitem circular simultâneamente em ambos os mundos e conectar as novas necessidades de negócio com
todo arcabouço de computação, matemática e estatística que existe na atualidade.

Sua opinião nos importa **muito**. Entre em contado quando quiser `admin@prognoos.com`
