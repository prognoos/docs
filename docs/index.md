# Prognoos

O objetivo desse documento é explicar objetivamente como integrar sua loja/Plataforma com nossa API. Após a validação da integração,
nossos algoritmos passarão a processar as informações coletadas para relacionar pessoas com produtos. O resultado desse processamento
será ficará disponível pra qualquer cliente exibir as informações processadas da forma que for mais conveniente.

Existe um ciclo de vida padrão no proceso de personalização de uma loja virtual:

1. Integração com serviço de tracking
2. Integração com serviço de vitrine

## Serviços de Tracking

* [Tracking de interação](integracao/tracking.md)

## Vitrines personalizadas

* [Vitrines dinâmicas e personalizadas](integracao/vitrine.md)

## Canais de comunicação

* [Integração no site](integracao/vitrine.md)
* Email (não disponível)

## Integração

Para iniciar o processo de integração siga [nossa documentação](integracao.md)

## Contato

Qualquer dúvida entre em contato `admin@prognoos.com` 
